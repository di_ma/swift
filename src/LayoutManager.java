import javax.swing.*;
import java.awt.*;

public class LayoutManager {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setSize(600, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);

        frame.setLayout(new BorderLayout());

        JButton button = new JButton("the button1");
        JTextField textField = new JTextField(10);
        JButton button1 = new JButton("The button 2");
        JButton button2 = new JButton("The button 3");
        JButton button3 = new JButton("The button4");
        JButton button4 = new JButton("The button 5");


        JPanel panel = new JPanel(new FlowLayout());

        panel.setPreferredSize(new Dimension(600, 100)); // устанавливаем новый размер панели, именно панели
        panel.setBackground(Color.GREEN);

        panel.add(button);
        panel.add(textField);
        panel.add(button1);
        panel.add(button2);
        panel.add(button3);
        panel.add(button4);

        frame.add(panel, BorderLayout.SOUTH);



//JButton []buttons = new JButton[10];
//
//for (int i = 0; i<buttons.length; i++){
//    buttons[i] = new JButton(String.valueOf(i));
//    frame.add(buttons[i]);
//}



        frame.setVisible(true);
    }
}
